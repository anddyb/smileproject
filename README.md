
# How to start app?

* Requirements: 
  * Maven
  * Java 11
  * Postgres 11.6 running on localhost:5432

* Database init commands:
  * CREATE DATABASE smileproject_database;
  * CREATE USER smileproject_user WITH PASSWORD 'smileprojectpasswork';
  * GRANT ALL PRIVILEGES ON DATABASE "smileproject_database" to smileproject_user;


* Step 1 
  * mvn clean package
    * (executable JAR will be in ./target/quarkus-app/quarkus-run.jar)

* Step 2 
  * java -Dquarkus.profile=dev -Xdebug -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -Xms512m -Xmx1024m -jar .\target\quarkus-app\quarkus-run.jar

# Test URLs

* /v1/task/all
* /v1/task/all?sortBy=createdDate
* /v1/task/all?sortBy=createdDate&departmentId=1
