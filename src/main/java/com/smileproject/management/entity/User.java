package com.smileproject.management.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "smileuser")//user is reserved word in PostgreSQL
@Entity
public class User {

    @Id
    @SequenceGenerator(name = "user_id_gen", sequenceName = "smileuser_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_gen")
    Long id;

    String name;

    @ManyToOne(fetch = FetchType.EAGER)
    Department department;

    //enriched from 3rd party service
    @Transient
    String rating;
}
