package com.smileproject.management;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.MediaType;
import kong.unirest.GenericType;
import kong.unirest.HeaderNames;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@ApplicationScoped
public class UserEnricher {

    @NonFinal
    @ConfigProperty(name = "user.rating.service.url")
    String userRatingServiceUrl;

    public Map<Long, String> findUserRatings(Set<Long> userIds) {

        try {
             var response = requestRatings(userIds);
            if (response.isSuccess()) {
                return response.getBody();
            }
            log.warn("Failed to get user ratings with HTTP code {}", response.getStatus());
        } catch (Exception e) {
            log.warn("Failed to get user ratings with an exception", e);
        }
        //TODO: process different responses depends on 3rd party service specification
        return Collections.emptyMap();
    }

    private HttpResponse<Map<Long, String>> requestRatings(Set<Long> userIds) {
        return Unirest.post(userRatingServiceUrl + "/ratings")
            .header(HeaderNames.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .body(userIds)
            .asObject(new GenericType<>() {
            });
    }
}
