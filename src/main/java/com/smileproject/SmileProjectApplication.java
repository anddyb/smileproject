package com.smileproject;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;

@OpenAPIDefinition(
    info = @Info(
        title = "Smile Project",
        version = "1.0.0"
    )
)
@ApplicationPath("/")
public class SmileProjectApplication extends Application {

}
