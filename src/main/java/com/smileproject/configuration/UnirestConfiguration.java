package com.smileproject.configuration;

import io.quarkus.runtime.Startup;
import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import kong.unirest.Unirest;

@Startup
@Singleton
public class UnirestConfiguration {

    @PostConstruct
    public void init() {

        Unirest.config()
            .connectTimeout(2000);//inMillies
    }
}
