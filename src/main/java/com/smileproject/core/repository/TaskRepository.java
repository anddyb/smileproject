package com.smileproject.core.repository;

import com.smileproject.core.entity.Task;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Sort;
import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Transactional
@ApplicationScoped
public class TaskRepository implements PanacheRepository<Task> {

    static final String QUERY_FIND_TASKS_BY_DEPARTMENT = "from Task as tsk "
        + "left join fetch tsk.author as ath "
        + "left join fetch ath.department as dptm "
        + "where dptm.id = ?1";

    public PanacheQuery<Task> findByDepartmentId(Long departmentId, Sort sort) {
        return find(QUERY_FIND_TASKS_BY_DEPARTMENT, sort, departmentId);
    }
}
