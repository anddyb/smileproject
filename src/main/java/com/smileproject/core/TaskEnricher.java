package com.smileproject.core;

import com.smileproject.core.entity.Task;
import com.smileproject.management.UserEnricher;
import com.smileproject.management.entity.User;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@ApplicationScoped
public class TaskEnricher {

    static final String DEFAULT_USER_RATING = "Junior";

    UserEnricher userEnticher;

    public void enrich(Task task) {

        enrich(Collections.singletonList(task));
    }

    public void enrich(List<Task> tasks) {

        enrichUsers(tasks);
    }

    private void enrichUsers(List<Task> tasks) {

        var allUsersForEnrichment = tasks.stream()
            .map(this::getUserIds)
            .flatMap(List::stream)
            .collect(Collectors.toSet());

        var userRatings = userEnticher.findUserRatings(allUsersForEnrichment);

        tasks.forEach(task -> enrichUsers(task, userRatings));
    }

    private void enrichUsers(Task task, Map<Long, String> userRatings) {

        enrichUser(task.getAuthor(), userRatings);
        enrichUser(task.getAssignee(), userRatings);
    }

    private void enrichUser(User user, Map<Long, String> userRatings) {

        if (user == null) {
            return;
        }

        var userRating = userRatings.getOrDefault(user.getId(), DEFAULT_USER_RATING);

        user.setRating(userRating);
    }

    private List<Long> getUserIds(Task task) {

        var authorId = task.getAuthor().getId();
        return task.getAssignee() == null ? List.of(authorId) : List.of(authorId, task.getAssignee().getId());
    }
}
