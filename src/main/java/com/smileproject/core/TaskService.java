package com.smileproject.core;

import com.smileproject.core.entity.Attachment;
import com.smileproject.core.entity.AttachmentData;
import com.smileproject.core.entity.Comment;
import com.smileproject.core.entity.Task;
import com.smileproject.core.entity.enums.TaskStatus;
import com.smileproject.core.repository.TaskRepository;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@ApplicationScoped
public class TaskService {

    TaskRepository taskRepository;
    TaskEnricher taskEnricher;
    AttachmentService attachmentService;

    @Transactional
    public Long create(@Valid @NotNull Task task) {

        task.setStatus(TaskStatus.OPEN);

        taskRepository.persistAndFlush(task);
        return task.getId();
    }

    @Transactional
    public Task update(@Valid @NotNull Task task) {

        var updatedTask = taskRepository.getEntityManager().merge(task);

        taskEnricher.enrich(updatedTask);

        return updatedTask;
    }

    public Task findOne(Long taskId) {

        var taskOptional = taskRepository.findByIdOptional(taskId);
        if (taskOptional.isEmpty()) {
            throw new WebApplicationException("Task not found", Status.NOT_FOUND);
        }

        var task = taskOptional.get();

        taskEnricher.enrich(task);

        return task;
    }

    /**
     *
     * TODO
     *
     * Depends on requirements this method should be refactored:
     * - use CriteriaBuilder or something similar
     * - delegate search to search engine (ElasticSearch, Solr, etc.)
     * - in any case for usage in PROD, either pagination or hardcoded limit for requested results is required
     */
    public List<Task> findAll(Long departmentId, String sortBy) {

        var sort = Sort.descending(sortBy);

        PanacheQuery<Task> query;
        if (departmentId == null) {
            query = taskRepository.findAll(sort);
        } else {
            query = taskRepository.findByDepartmentId(departmentId, sort);
        }

        var tasks = query.list();

        taskEnricher.enrich(tasks);

        return tasks;
    }

    @Transactional
    public Long addComment(Long taskId, @Valid @NotNull Comment comment) {

        var task = findOne(taskId);

        //TODO: set comment's author based on auth info

        var comments = task.getComments();
        if (comments == null) {
            comments = new ArrayList<>();
        }
        comments.add(comment);
        task.setComments(comments);
        taskRepository.getEntityManager().merge(task);

        return comment.getId();
    }

    /**
     *
     * TODO
     *
     * This method can be changed to mark comments as removed instead of actual removal.
     * It would be safer for some cases - but depends on requirements
     */
    @Transactional
    public boolean deleteComment(Long taskId, Long commentId) {

        var task = findOne(taskId);

        var comments = task.getComments();

        if (comments == null) {
            throw commentNotFound();
        }

        var result = comments.removeIf(comment -> comment.getId().equals(commentId));
        if (result) {
            taskRepository.getEntityManager().merge(task);
            return true;
        }
        throw commentNotFound();
    }

    private WebApplicationException commentNotFound() {
        return new WebApplicationException("Task doesn't have a comment with the specified id", Status.NOT_FOUND);
    }

    @Transactional
    public Long addAttachment(Long taskId, AttachmentData attachmentData) {

        var task = findOne(taskId);

        String fileId = attachmentService.saveAttachmentToStorage(attachmentData);
        var attachment = new Attachment();
        attachment.setStorageFileId(fileId);

        var attachments = task.getAttachments();
        if (attachments == null) {
            attachments = new ArrayList<>();
        }
        attachments.add(attachment);
        task.setAttachments(attachments);
        taskRepository.getEntityManager().merge(task);

        return attachment.getId();
    }
}
