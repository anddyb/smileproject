package com.smileproject.core.entity.enums;

public enum TaskStatus {

    OPEN,
    IN_PROGRESS,
    CLOSED
}
