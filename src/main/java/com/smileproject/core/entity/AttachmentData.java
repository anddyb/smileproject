package com.smileproject.core.entity;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.jboss.resteasy.annotations.providers.multipart.PartType;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AttachmentData {

    @FormParam("file")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    byte[] file;
}
