package com.smileproject.core.entity;

import com.smileproject.management.entity.User;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotEmpty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Comment {

    @Id
    @SequenceGenerator(name = "comment_id_gen", sequenceName = "comment_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comment_id_gen")
    Long id;

    @NotEmpty
    String message;

    @OneToOne(fetch = FetchType.EAGER)
    User author;
}
