package com.smileproject.core.entity;

import com.smileproject.core.entity.enums.TaskStatus;
import com.smileproject.management.entity.User;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Task {

    @Id
    @SequenceGenerator(name = "task_id_gen", sequenceName = "task_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "task_id_gen")
    Long id;

    @CreationTimestamp
    @Column(updatable = false)
    LocalDateTime createdDate;

    @NotEmpty
    String subject;

    String description;

    @NotNull
    @OneToOne(fetch = FetchType.EAGER)
    User author;

    @OneToOne(fetch = FetchType.EAGER)
    User assignee;

    @Enumerated(EnumType.STRING)
    TaskStatus status;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Comment> comments;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Attachment> attachments;
}
