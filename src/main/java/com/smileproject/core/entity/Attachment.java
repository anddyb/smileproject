package com.smileproject.core.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Attachment {

    @Id
    @SequenceGenerator(name = "attachment_id_gen", sequenceName = "attachment_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "attachment_id_gen")
    Long id;

    String storageFileId;
}
