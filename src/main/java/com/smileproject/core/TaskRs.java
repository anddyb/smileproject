package com.smileproject.core;

import static javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA;

import com.smileproject.core.entity.AttachmentData;
import com.smileproject.core.entity.Comment;
import com.smileproject.core.entity.Task;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@ApplicationScoped
@Path("/v1/task")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TaskRs {

    TaskService taskService;

    @POST
    public Long create(Task task) {

        return taskService.create(task);
    }

    @PUT
    public Task update(Task task) {

        return taskService.update(task);
    }

    @GET
    @Path("/{taskId}")
    public Task findOne(@PathParam("taskId") Long taskId) {

        return taskService.findOne(taskId);
    }

    @GET
    @Path("/all")
    public List<Task> findAll(@QueryParam("departmentId") Long departmentId,
        @DefaultValue("subject") @QueryParam("sortBy") String sortBy) {

        return taskService.findAll(departmentId, sortBy);
    }

    @POST
    @Path("/{taskId}/comment")
    public Long addComment(@PathParam("taskId") Long taskId, Comment comment) {

        return taskService.addComment(taskId, comment);
    }

    @DELETE
    @Path("/{taskId}/comment/{commentId}")
    public boolean deleteComment(@PathParam("taskId") Long taskId, @PathParam("commentId") Long commentId) {

        return taskService.deleteComment(taskId, commentId);
    }

    @POST
    @Consumes(MULTIPART_FORM_DATA)
    @Path("/{taskId}/attachment")
    public Long addAttachment(@PathParam("taskId") Long taskId, @MultipartForm AttachmentData attachmentData) {

        return taskService.addAttachment(taskId, attachmentData);
    }
}