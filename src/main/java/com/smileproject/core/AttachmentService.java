package com.smileproject.core;

import com.smileproject.core.entity.AttachmentData;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@ApplicationScoped
public class AttachmentService {

    public String saveAttachmentToStorage(AttachmentData attachmentData) {

        //TODO: Store file to some storage as AWS S3 or Azure Blob

        return UUID.randomUUID().toString();
    }
}
