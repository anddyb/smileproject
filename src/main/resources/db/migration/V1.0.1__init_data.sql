------------------- Department -------------------

INSERT INTO department (id, name)
VALUES (NEXTVAL('department_seq'), 'Department A');

INSERT INTO department (id, name)
VALUES (NEXTVAL('department_seq'), 'Department B');

INSERT INTO department (id, name)
VALUES (NEXTVAL('department_seq'), 'Department C');


------------------- User -------------------

INSERT INTO smileuser (id, name, department_id)
VALUES (NEXTVAL('smileuser_seq'), 'User 1', (SELECT dpm.id FROM department AS dpm WHERE name = 'Department A'));

INSERT INTO smileuser (id, name, department_id)
VALUES (NEXTVAL('smileuser_seq'), 'User 2', (SELECT dpm.id FROM department AS dpm WHERE name = 'Department C'));