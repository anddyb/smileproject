
------------------- Attachment -------------------
CREATE TABLE attachment
(
    id            BIGINT NOT NULL
        CONSTRAINT attachment_pkey
            PRIMARY KEY,
    storagefileid VARCHAR(255)
);

ALTER TABLE attachment
    OWNER TO smileproject_user;
    
CREATE SEQUENCE attachment_seq
	INCREMENT BY 50;
ALTER SEQUENCE attachment_seq OWNER TO smileproject_user;

------------------- Department -------------------

CREATE TABLE department
(
    id   BIGINT NOT NULL
        CONSTRAINT department_pkey
            PRIMARY KEY,
    name VARCHAR(255)
);

ALTER TABLE department
    OWNER TO smileproject_user;
    
CREATE SEQUENCE department_seq
	INCREMENT BY 50;

ALTER SEQUENCE department_seq OWNER TO smileproject_user;

------------------- User -------------------

CREATE TABLE smileuser
(
    id            BIGINT NOT NULL
        CONSTRAINT smileuser_pkey
            PRIMARY KEY,
    name          VARCHAR(255),
    department_id BIGINT
        CONSTRAINT fk_smileuser_department
            REFERENCES department
);

ALTER TABLE smileuser
    OWNER TO smileproject_user;
    
CREATE SEQUENCE smileuser_seq
	INCREMENT BY 50;

ALTER SEQUENCE smileuser_seq OWNER TO smileproject_user;

------------------- Comment -------------------

CREATE TABLE comment
(
    id        BIGINT NOT NULL
        CONSTRAINT comment_pkey
            PRIMARY KEY,
    message   VARCHAR(255),
    author_id BIGINT
        CONSTRAINT fk_comment_author
            REFERENCES smileuser
);

ALTER TABLE comment
    OWNER TO smileproject_user;
    
CREATE SEQUENCE comment_seq
	INCREMENT BY 50;

ALTER SEQUENCE comment_seq OWNER TO smileproject_user;

------------------- Task -------------------

CREATE TABLE task
(
    id          BIGINT NOT NULL
        CONSTRAINT task_pkey
            PRIMARY KEY,
    createddate TIMESTAMP,
    description VARCHAR(255),
    status      VARCHAR(255),
    subject     VARCHAR(255),
    assignee_id BIGINT
        CONSTRAINT fk_task_assignee_smileuser
            REFERENCES smileuser,
    author_id   BIGINT
        CONSTRAINT fk_task_author_smileuser
            REFERENCES smileuser
);

ALTER TABLE task
    OWNER TO smileproject_user;
    
CREATE SEQUENCE task_seq
	INCREMENT BY 50;

ALTER SEQUENCE task_seq OWNER TO smileproject_user;

CREATE TABLE task_attachment
(
    task_id        BIGINT NOT NULL
        CONSTRAINT fk_task_attachment_task
            REFERENCES task,
    attachments_id BIGINT NOT NULL
        CONSTRAINT uk_task_attachment_attachment
            UNIQUE
        CONSTRAINT fk_task_attachment_attachment
            REFERENCES attachment
);

ALTER TABLE task_attachment
    OWNER TO smileproject_user;

CREATE TABLE task_comment
(
    task_id     BIGINT NOT NULL
        CONSTRAINT fk_task_comment_task
            REFERENCES task,
    comments_id BIGINT NOT NULL
        CONSTRAINT uk_task_comment_comment
            UNIQUE
        CONSTRAINT fk_task_comment_comment
            REFERENCES comment
);

ALTER TABLE task_comment
    OWNER TO smileproject_user;
